//Clase logger permite la visualizacion de los puntos anotados
// Diego Tenorio Maestre 52193

#include <sys/fcntl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

#define TAM 56

int main(void){
	int fd;
	const char *myfifo="/tmp/myfifo";

	char buf[TAM]; //buf es un puntero a char

	if(mkfifo(myfifo,0777)!=0){ //Creamos la fifo
		perror("mkfifo");
		exit(1);
	}

	fd=open(myfifo,O_RDONLY);  //Abrimos la fifo SOLO para LECTURA

	if(fd==-1){
		perror("open fifo");
		exit(1);
	}

	while(1){								//Bucle infinito
		if(read(fd,&buf,TAM*sizeof(char))<1){
			perror("read from fifo");
			exit(1);
		}

		if(write(1,buf,TAM*sizeof(char))<1){  //Muestra el mensaje por la salida estandar
			perror("write from fifo");
			exit(1);
		}
	}

	if(close(fd)!=0 || unlink(myfifo)!=0){
		perror("close fifo");
		exit(1);
	}

	return 0;
}
